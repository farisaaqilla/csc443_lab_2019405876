<html>
<body>
<?php

//Question 5a
$monthDays = array (
    'Splorch' => 23, 
    'Sploo' => 28,
    'Splat' => 2, 
    'Splatt' => 3,
    'Spleen' => 44, 
    'Splune' => 30,
    'Spling' => 61, 
    'Slendo' => 61,
    'Sploctember' => 31,
    'Splictember' => 31,
    'Splanet' => 30, 
    'TheRest' => 22);

//Question 5b(i)
    echo "Number of days in the shortest month: " . min($monthDays);
    echo "</br>";

//Question 5b(ii)
    echo "Name of the shortest month: " . array_search(min($monthDays),$monthDays);
    echo "</br>";
//Question 5b(iii)  
    echo "Total number of days in a year: ". array_sum($monthDays) ;

?>
</body>
</html>

