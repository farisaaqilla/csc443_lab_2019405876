<html>
<body>

<?php

//Question 4a
$monthDays = array (
    'January' => 31, 
    'February' => 28,
    'March' => 31, 
    'April' => 30,
    'May' => 31, 
    'June' => 30,
    'July' => 31, 
    'August' => 31,
    'September' => 30, 
    'October' => 31,
    'November' => 30, 
    'December' => 31);

//Question 4b
    foreach ($monthDays as $key => $val) {
        echo "Number of days in $key is : " . $val;
        echo "<br>"; 
    }

//Question 4c
echo "</br>";
echo "List of months that has 30 days are : "; 
echo "</br>";

foreach ($monthDays as $key => $val) {
    if ($val ==30)
    {   echo $key; 
        echo "</br>";
    }
}

?>
</body>
</html>

